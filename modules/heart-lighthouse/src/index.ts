import { LighthouseModule } from "./LighthouseModule.js"

export default new LighthouseModule({
  name: "Heart Lighthouse",
  service: {
    name: "Google Lighthouse",
    logo: "https://gitlab.com/fabernovel/heart/raw/master/assets/images/logos/Lighthouse.png?v=20190916",
  },
})
