import { GreenITModule } from "./GreenITModule"

export default new GreenITModule({
  name: "Heart GreenIT",
  service: {
    name: "GreenIT Analysis",
    logo: "https://gitlab.com/fabernovel/heart/raw/master/assets/images/logos/GreenIT.png",
  },
})
