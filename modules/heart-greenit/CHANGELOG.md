# Change Log - @fabernovel/heart-greenit

This log was last generated on Wed, 27 Jul 2022 13:43:15 GMT and should not be manually modified.

## 3.0.0
Wed, 27 Jul 2022 13:43:15 GMT

### Breaking changes

- Initial release of the GreenIT module

